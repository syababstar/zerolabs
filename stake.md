Staking requirements:
Before you can stake your ZERO you will need to meet all of these requirements:
A ZERO balance exceeding 1 ZERO with at least 300 confirmations.
A ZERO wallet running the latest ZERO release and synchronized to the latest block.
The wallet needs to be unlocked for staking.
The wallet also needs to be open and connected to the internet 24/7 to stake.

1.Connecting to the network
-Download zero-windows.zip or zero-linux.tar.gz on https://zeroprotocol.org or our repo
-Extract the file, do not open it yet
-On your keyboard, Windows + R then %appdata%, create a folder and name it to Zero. Drop the file zero.conf on the folder that you extracted above to Zero folder on %appdata%
-Open zero-qt.exe on the extracted wallet folder, it will take few minutes to few hours based on your connection to sync the blockchain.
2.Start staking
-Unlock your wallet, tick Staking only function then leave it.
-Block production is starting, Congratulation !
