Submit your first transaction

Please see stake.md for instructions on participating in the Mainnet 1.0!

Zero is a side-chain. Expect substantial changes before the release.


Zero is a privacy-preserving network designed for cross-chain assets transfer


Zero is available under open-source licenses. Look for the LICENSE file in each crate for more information.

Cryptography Notice

This distribution includes cryptographic software. Your country may have restrictions on the use of encryption software. Please check your country's laws before downloading or using this software.
Overview

Zero is a privacy network without any central authority. The fundamental goal of the network is to safely and efficiently enable the exchange of value, represented as fractional ownership of the total value of the network. Cryptography is used extensively to establish ownership, control transfers, and to preserve cash-like privacy for users.

Here we review a few design concepts that are essential for understanding the blockchain

Transactions

The total value of the Zero network is fixed by convention at a sum of 150 million ZERO.

For more information on how transactions work, and how they use CrytpoNote-style transactions to preserve privacy of both the sender and receiver, see the transaction crate.

Consensus : Hybrid PoW/PoS -Smart contract ( cross-chain bridge )

A hybrid PoW/PoS allows for both proof-of-stake and proof-of-work as consensus distribution algorithms on the network. This approach aims to bring together the security of PoW consensus and the governance and energy efficiency of PoS. PoW will end after block 1000 minted and fully start PoS progress.

Running a node on the network will automatically participate in the validation of blocks and transmission of blocks, while your wallet constantly evaluates your personal ZERO transactions to see if you win the right to create the next block.

The bridge enable users to swap ZERO and other assets on supported chain to ZERO side-chain then to other chain with automatic data sync at the swap time and smart contract on different chain will work in any time to protect your identity .  
Eg : You swap 1 ETH on BEP20 network to HRC20 network, the price data will be noted and convert to native ZERO on Zero side-chain then flow to other  HRC20 address , means 1 ETH BEP20 always equal 1 ETH HRC20 but no one can view that is your ETH.

How to start staking ?

Staking requirements:

Before you can stake your ZERO you will need to meet all of these requirements:
-A ZERO balance exceeding 0 ZERO with at least 300 confirmations.
-A ZERO wallet running the latest ZERO release and synchronized to the latest block.
-The wallet needs to be unlocked for staking.
-The wallet also needs to be open and connected to the internet 24/7 to stake.

Get Started : Please view configuration on stake.md
